import 'package:flutter/material.dart';

class Question extends StatelessWidget {
  String _questionText = '';
  Question({super.key, required String text}) {
    _questionText = text;
  }

  @override
  Widget build(BuildContext context){
    return Container(
      width: double.infinity,
      margin: const EdgeInsets.symmetric(vertical: 50, horizontal: 0),
      child: Text(
        _questionText,
        textAlign: TextAlign.center,
        style: const TextStyle(
          fontSize: 35,
          color: Colors.blue,
        )
      )
    );
  }
}