import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lab_02/answer.dart';
import 'package:lab_02/question.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  var _questionIndex = 0;

  var questions = [
    {
      'question': 'Select shirt type',
      'answer': [
        'T-Shirt',
        'Shirt',
        'Sweatshirt'
      ]
    },
    {
      'question': 'Select pants type',
      'answer': [
        'Slim',
        'Skinny',
        'Mom',
        'Over-sized',
        'Regular'
      ]
    },
    {
      'question': 'Select shoe type',
      'answer': [
        'Basketball',
        'Running',
        'Lifestyle',
        'Football',
        'Boxing',
        'Tennis',
        'Boots'
      ]
    }
  ];

  void _buttonPressed() {
    setState(() {
      _questionIndex += 1;
    });
    print('tapped$_questionIndex');
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Fashion Shop',
        home: Scaffold(
            appBar: AppBar(
              title: const Text('Fashion Shop'),
            ),
            body: Column(children: [
              Question(text: questions[_questionIndex]['question'].toString()),
              ...(questions[_questionIndex]['answer'] as List<String>)
                  .map((answer) {
                return Answer(pressed: _buttonPressed, text: answer);
              }),
            ])
        )
    );
  }
}