import 'package:flutter/material.dart';

class Answer extends StatelessWidget {
  String _answerText = '';
  Function()? _buttonPressed = () {};

  Answer({super.key, required Function()? pressed, required String text}) {
    _answerText = text;
    _buttonPressed = pressed;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.symmetric(vertical: 10),
        child: ElevatedButton(
            style: ButtonStyle(
              backgroundColor: MaterialStateProperty.all(Colors.green),
            ),
            onPressed: _buttonPressed,
            child: Text(
              _answerText,
              style: const TextStyle(color: Colors.red),
            )
        )
    );
  }
}